insert into category values ('Mascarillas','Quirurjicas','1')
insert into category values ('Durex','Sabor sandia','1')
insert into category values ('Piel','Sabor fresa','1')
insert into category values ('Sildenafil','51mg','1')
insert into category values ('Aspirina','111mg','1')
insert into category values ('Furosemida','51mg','1')
insert into category values ('Apronax','51mg','1')
insert into category values ('Panadol','511mg','1')
insert into category values ('Airun jat','51ml','1')
insert into category values ('Azitromicina','51mg','1')

insert into usuarioStatus values (1,'activo')
insert into usuarioStatus values (0,'Inactivo')

insert into productoStatus values (1,'activo')
insert into productoStatus values (0,'Inactivo')

insert into rack values ('rack 1','piso 1')
insert into rack values ('rack 1','piso 2')
insert into rack values ('rack 1','piso 3')
insert into rack values ('rack 1','piso 4')
insert into rack values ('rack 1','piso 5')

insert into rack values ('rack 2','piso 1')
insert into rack values ('rack 2','piso 2')
insert into rack values ('rack 2','piso 3')
insert into rack values ('rack 2','piso 4')
insert into rack values ('rack 2','piso 5')

insert into rack values ('rack 3','piso 1')
insert into rack values ('rack 3','piso 2')
insert into rack values ('rack 3','piso 3')
insert into rack values ('rack 3','piso 4')
insert into rack values ('rack 3','piso 5')

insert into rack values ('rack 4','piso 1')
insert into rack values ('rack 4','piso 2')
insert into rack values ('rack 4','piso 3')
insert into rack values ('rack 4','piso 4')
insert into rack values ('rack 4','piso 5')

insert into rack values ('rack 5','piso 1')
insert into rack values ('rack 5','piso 2')
insert into rack values ('rack 5','piso 3')
insert into rack values ('rack 5','piso 4')
insert into rack values ('rack 5','piso 5')


insert into producto values('Ramipril','32','25.60','Internacional lab','2022-11-30','Nutricia',167,'preservativos','2','2','8');
insert into producto values('Aspirina','43','34.40','Sindinext','2022-11-30','Gents',37,'injectables','2','2','24');
insert into producto values('BigM chocolate','15','12.00','Internacional lab','2022-11-30','3M',182,'suero','1','4','20');
insert into producto values('BigM vainilla','29','23.20','Internacional lab','2022-11-30','Piel',154,'pastilla','1','6','14');
insert into producto values('Simvastatina ','177','141.60','Suiza Lab','2022-11-30','Gents',146,'jarabe','2','7','15');
insert into producto values('Amlodipina','68','54.40','Washington lab','2022-11-30','Teva',163,'injectables','2','1','3');
insert into producto values('Amlodipina','27','21.60','Internacional lab','2022-11-30','Pfizer',143,'suero','1','5','4');
insert into producto values('Omeprazol','99','79.20','Monsanto','2022-11-30','Almirall',60,'pastilla','1','2','1');
insert into producto values('Amlodipina','87','69.60','Multilab','2022-11-30','Pfizer',58,'suero','2','3','22');
insert into producto values('Freegen','155','124.00','Suiza Lab','2022-11-30','Pfizer',122,'suero','2','3','24');
insert into producto values('Ramipril','34','27.20','Medlab','2022-11-30','Nutricia',45,'pastilla','2','9','20');
insert into producto values('BigM vainilla','128','102.40','Internacional lab','2022-11-30','Pfizer',17,'pastilla','2','2','1');
insert into producto values('BigM vainilla','179','143.20','Monsanto','2022-11-30','BioGeriTec',117,'jarabe','1','7','9');
insert into producto values('BigM chocolate','183','146.40','Llenerig','2022-11-30','BioGeriTec',110,'injectables','2','8','11');
insert into producto values('Laxante','184','147.20','Medlab','2022-11-30','Pfizer',36,'jarabe','2','6','4');
insert into producto values('BigM chocolate','123','98.40','Sindinext','2022-11-30','Almirall',131,'injectables','1','1','11');
insert into producto values('Laxante','123','98.40','Internacional lab','2022-11-30','Wyeth',80,'suero','1','7','25');
insert into producto values('Ramipril','30','24.00','Sindinext','2022-11-30','3M',71,'preservativos','1','9','5');
insert into producto values('Omeprazol','121','96.80','Sulabsa','2022-11-30','3M',123,'injectables','1','6','21');
insert into producto values('BigM vainilla','65','52.00','Suiza Lab','2022-11-30','Wyeth',14,'injectables','2','9','8');
insert into producto values('Laxante','168','134.40','Suiza Lab','2022-11-30','Teva',110,'suero','1','4','4');
